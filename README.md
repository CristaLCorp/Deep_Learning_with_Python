# Deep Learning with Python

My notes about the [book](https://www.manning.com/books/deep-learning-with-python) Deep Learning with Python

## Chapter 1 : What is deep learning ?
### Artificial Intelligence, machine learning, and deep learning
#### Artificial Intelligence
tapis lasso fusee
large set of rules -> Symbolic AI -> expert systems
#### Machine learning
"Lady Lovelace's Objection" Alan Turing
Classical programming : (Rules + Data) = Answers
Machine learning : (Data + Answers) = Rules
#### Learning representations from data
* Inputs data
* Example of the expected output
* A way to mesure whether the algorithm is doing a good job

The format or representation is of paramount importance and has to fit the results we are looking for (HSV vs RGB)
"learning" describes an automatic search process for better representations
Machine learning algorithms aren't usually creative in finding theses transformations -> hypothesis space
#### The "deep" in deep learning
successive layers of representations (n layers = depth)
only 1 or 2 layers -> shallow learning
the layers are learned via *neural networks* models
deep learning is a mathematical framework for learning representations of data.
deep network : *multistage information-distillation operation, where information goes through successive filters and comes out increasingly purified*
#### Understanding how deep learning works, in three figures
* The transformation implemented by a layer is parameterized by its weights (initially randomly assigned)
* Loss function (or objective function) measures the quality of the network's output
* This score is used as a feedback signal to adjust the weights
### Before deep learning: a brief history of machine learning
#### Probability modeling
is the principe of statistics applied to data analysis
*Naive Bayes* assuming that the features in the input data are all independent
*Logistic regression* (logreg) classification algo. often used to get a feel for the classification task at hand
#### Early neural networks
rediscovering the Backpropagation algorithm to train large neural networks using gradient-descent optimization was the missing piece.
Yann LeCunn combined the ideas of convolutional neural networks and backpropagation -> used by USPS to read ZIP code during the 1990s.
#### Kernel methods
classification algorithm, most known is *support vector machine* (SVM). Aiming at finding good decision boundaries
* mapping data to a new high-dimensional representation where the decision boundary can be expressed as a hyperplane
* a good hyperplane is computed by trying to maximise the distance between the hyperplane and the closest data points from each class -> maximizing the margin
kernel trick : compute the distance between pairs of points in that space (using a kernel function).
Kernel function are crafted by hand, only the separation hyperplane is learned.
#### Decision trees, random forest, and gradient boosting machines
Flowchart-like structure -> successive "questions" to classify data
Random forest : large number of specialized decision trees, ensembling their output
Gradient boosting : iteratively train new models that specialize in addressing the weak points of the previous models (best algo for nonperceptual data, today)
#### Back to neural networks
Deep convolutional neural networks (convnets)
CERN : Keras-based deep neural networks
#### What makes deep learning different
Performance in perceptual fields (vision, writing,...)
2 essentials points : *The incremental, layer-by-layer way in which increasingly complex representations are developed* and *the fact that these intermediate incremental representations are learned jointly*
#### The modern machine-learning landscape
Gradient boosting is used for problems where structured data is available (XGBoost lib)
Deep learning is used for perceptual problems such as image classification (Keras)
### Why deep learning ? Why now ?
#### Hardware
#### Data
#### Algorithms
#### A New wave of investment
#### The democratization of deep learning
#### Will it last ?

## Chapter 2 : The mathematical building blocks of neural networks
### A first look at a neural network
MNIST is the "Hello World" of deep learning
### Data representations for neural networks
Tensors are multidimensional Numpy arrays. They are a generalization of matrices to an arbitrary number of dimensions.
Dimensions == Axes == Rank
#### Scalars (0D tensors)
A tensor with only one number is a *scalar* tensor or 0D tensor.
0 axes
x = numpy.array(12)
x.ndim >>> 0
#### Vectors (1D tensors)
An array of number is called a *vector* or 1D tensor. It has 4 entries, so it is also a 4-dimensional vector
1 axe
x = np.array([12, 3, 6, 14])
x.ndim >>> 1
#### Matrices (2D tensors)
An array of vectors is a matrix or 2D tensor.
2 axes
x = np.array([[12, 3, 6, 14],
[6, 78, 1, 92],
[7, 69, 0, 27]])
x.ndim >>> 2
The entries from the first axis/dimension are called rows : [12, 3, 6, 14]
The entries from the second axis/dimension are called columns : [12, 6, 7]
#### 3D tensors and higher-dimensional tensors
An array of matrices is a 3D tensor, a "cube of numbers".
3 axes
x = np.array([[[12, 3, 6, 14],
[6, 78, 1, 92],
[7, 69, 0, 27]],
[[12, 3, 6, 14],
[6, 78, 1, 92],
[7, 69, 0, 27]],
[[12, 3, 6, 14],
[6, 78, 1, 92],
[7, 69, 0, 27]]])
x.ndim >>> 3
Packing 3D tensors in an array creates a 4D tensor, and so on. In deep learning you generally manipulate tensors between 0D and 4D (5D for video processing)
#### Key attributes
* Number of axis (rank)
* Shape : How manny dimensions the tensor has along each axis
* Data type or dtype : uint8, float32, float64... Tensors live in preallocated, contiguous memory segments : strings, being variable length, preclude the use of this implementation.
#### Manipulating tensors in Numpy
Works like lists in python.
train_images[10:100, :, :] == train_images[10:100, 0:28, 0:28] (in our example of 6000, 28*28)
" 10:100 " == 10 to 99 (100 excluded)
" : " == all
" 14: " == from 14 to the end
" 7:-7 " == from 7 to -7(relative to the end)
#### The notion of data batches
The first axis (axis 0) will be the *samples* axis. The number of "echantillons" we've got.
Deep-learning models break the data into small batches.
The first axis is also called the *batch axis* or *batch dimension*
#### Real-world examples of data tensors
* Vector data : 2D tensors, shape (samples, features)
* Timeseries data or sequence data : 3D tensors, shape (samples, timesteps, features)
* Images : 4D tensors, shape (samples, height, width, channels)
* Video : 5D tensors, shape (samples, frames, height, width, channels)
#### Vector data
2 axis
First axis is the sample axis. Second axis is the feature axis.
-> 100 000 people, 3 specs (zip, age, income) : (100 000, 3)
#### Timeseries data or sequence data
Encoded as a sequence of vector : 3 axis
First axis is the sample axis, second axis is the time axis, third axis is the feature axis
-> 250 days of trading, 1 day is 390 minutes, 3 specs : (250, *390*, 3)
-> 1G tweets of 280 char in a list of 128 possible : (1 000 000, 280, 128)
#### Image data
4 axis
samples, height, width, color_depth
-> 128 greyscale images of 256x256 : (128, 256, 256, 1)
-> colors : (128, 256, 256, 3)
#### Video data
5 axis
samples, frames, height, width, color_depth
4 x 60 sec youtube clip 144x256 4 fps : (4, 240, 144, 256, 3)
### The gears of neural networks: tensor operations
#### Element-wise operations
Operations that are applied independently to each entry in the tensor. Massively // implementations : Vectorized implementations -> Vector Processor Supercomputer archi 1970-90
#### Broadcasting
Operations on tensors of different size (ndim)
1 - Axes are added to the smaller tensor to match the ndim of the larger tensor (Broadcast axes).
2 - The smaller tensor is repeated alongside these new axis to match the full shape of the larger tensor.
(32, 10) + *(10,)* -> (32, 10) + *(1, 10)* -> (32, 10) + *(32, 10)*
No new 2D tensor is created, this operation is virtual, it happens at the algorithmic level, not at the memory level
#### Tensor dot
#### Tensor reshaping
#### Geometric interpretation of tensor operations
#### A geometric interpretation of deep learning
### The engine of neural networks: gradient-based optimization
#### What's a derivative ?
#### Derivative of a tensor operation: the gradient
#### Stochastic gradient descent
#### Chaining derivatives : the Backpropagation algorithm
### Looking back at our first example